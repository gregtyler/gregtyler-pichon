# Pichon files for gregtyler.co.uk

[gregtyler.co.uk](https://gregtyler.co.uk) is served through my bespoke blog engine [Palomar](https://gitlab.comm/gregtyler/palomar). Palomar uses extended [Markdown files](http://commonmark.org/) which I refer to as "pichon files" for each blog post and these are those files.

Each folder (apart from images) is a category on the blog, and each markdown file within is a post. Posts have header information at the top and then work as normal Markdown files, with a few extra bespoke bits of functionality.
